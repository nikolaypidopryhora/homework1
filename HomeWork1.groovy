​assert [[1,2,3,3,3,3,4]].first().unique() == [1,2,3,4]

assert ['dog', 'Roma']*.size() == [3, 4]

assert ['cat', 'dog'].size() == 2

def x = [1,2,3,4,5]
assert (x << 6) == [1,2,3,4,5,6]​

assert 2 == 'cat'